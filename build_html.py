#!/usr/bin/python3

from markdown import markdown

head = f"""
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Python Exercise</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/code.css">
  </head>
  <body>
    <main>
"""

foot = """
    </main>
  </body>
</html>
"""

with open('task.md') as fh:
    content = (markdown(fh.read(), extensions=['codehilite', 'toc'], extension_configs = { 'codehilite': { 'css_class': 'syntax' }}))

with open('./public/index.html', mode='w') as fh:
    fh.write(head + content + foot)

print("done")